package de.artismarti.rest;

import de.artismarti.data.Person;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
@RestController
@RequestMapping("/person")
public class PersonRestController {

	Map<Integer, Person> personMap = new HashMap<>();

	@PostConstruct
	private void init() {
		personMap.put(1, new Person(1L, "Art", "Smart", "ab@c.de"));
		personMap.put(2, new Person(2L, "Arti", "Smarti", "as@gmx.de"));
		personMap.put(3, new Person(3L, "xxx", "xxx", "abc@xxx.de"));
	}

	@RequestMapping("/all")
	public Collection<Person> getAll() {
		return personMap.values();
	}
}
